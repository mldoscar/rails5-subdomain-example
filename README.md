# README

In this example I've implemented how to set subdomains for your rails project. To do this I've run the following comands in this example:

```
#!bash

rails new rails5-subdomain-example
```


Then, you have to set a home controller for your main view
```
#!bash

rails g controller home index
```


After that, you might want to generate a new namespace. So in this case I generated an admin namespace, and also a panel controller inside of it

```
#!bash
rails g controller admin/panel index
```


Last but not least, you just configure routes.rb file:

**config/routes.rb**

```
#!ruby
Rails.application.routes.draw do
  # YOU CAN TEST SUBDOMAINS IN DEVELOPMENT MODE USING 'lvh.me'
  # INSTEAD OF 'localhost' IN YOUR BROWSER. Example:
  # http://yoursubdomainhere.lvh.me:3000

  # Automatically generated by home controller
  get 'home/index'

  # Admin namespace automatically generated
  namespace :admin do
    get 'panel/index'
  end

  # Set a subdomain constraint
  constraints(subdomain: 'admin') do
    # Route a namespace you want for your subdomain
    namespace :admin, path: '/' do
      # Route every controller and action you want for this subdomain in here
      get '/' => 'panel#index'
    end
  end
  
  # When subdomains constraints are declared, root directive
  # MUST be declared at the end
  root 'home#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end


```


Finally, you just start your rails server in development mode:

```
#!bash

rails s
```